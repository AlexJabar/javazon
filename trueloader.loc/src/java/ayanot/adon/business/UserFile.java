/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ayanot.adon.business;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

 
@Entity
public class UserFile implements Serializable {
    private static final long serialVersionUID = 1L;
    
    //@GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private String name;
    private String url;
    private String oldName;
    private String extension;
    private String fileSize;
    private String description;

    public UserFile() {
        this.name        = "";
        this.url         = "";
        this.oldName     = "";
        this.extension   = "";
        this.fileSize    = "";
        this.description = "";
    }

    public UserFile(String name, String url, String oldName, String extension, String fileSize, String description) {
        this.name = name;
        this.url = url;
        this.oldName = oldName;
        this.extension = extension;
        this.fileSize = fileSize;
        this.description = description;
    }

   

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOldName() {
        return oldName;
    }

    public void setOldName(String oldName) {
        this.oldName = oldName;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

  
    
    
    
    

    
}
