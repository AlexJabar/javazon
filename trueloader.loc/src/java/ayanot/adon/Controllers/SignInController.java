/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ayanot.adon.Controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ayanot.adon.business.UserPerson;
import ayanot.adon.DB.DBUser;
import ayanot.adon.Helpers.Auth;

/**
 *
 * @author HACKER
 */
@WebServlet("/signin")
public class SignInController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.getRequestDispatcher("/views/auth/signin.jsp").forward(request, response);
        request.getSession().removeAttribute("error");
        processRequest(request, response);
    }

   
   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String email      = request.getParameter("email");
        String password   = request.getParameter("password");
        
        
        boolean auth = Auth.attempt(email, password, request);
        
        if (!auth) {
             request.getSession().setAttribute("error", "Invalid Email or Password" );
             response.sendRedirect(request.getHeader("Referer"));
             
        } else {
        
            response.sendRedirect(request.getContextPath());
        }
        
        
        
        processRequest(request, response);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
