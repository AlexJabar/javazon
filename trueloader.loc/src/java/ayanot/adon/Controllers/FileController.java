
package ayanot.adon.Controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ayanot.adon.DB.DBFile;
import ayanot.adon.business.UserFile;
import java.net.URLDecoder;
import java.net.URLEncoder;


@WebServlet("/file/*")
public class FileController extends HttpServlet {

   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String[] pathParts = request.getPathInfo().split("/"); 
        String urlParam    = pathParts[1];//взять последнюю частт url
        
        UserFile file  = DBFile.fetchByName(urlParam);
        
        String url = request.getScheme() + "://" + request.getServerName() + ":" 
                                            + request.getServerPort()+"/files"
                                            + file.getUrl();
        
        
        request.setAttribute("url", url);
        request.setAttribute("name", file.getOldName() + file.getExtension());
        request.getRequestDispatcher("/views/fileDownload.jsp").forward(request, response);
        
        
        
        
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        processRequest(request, response);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
