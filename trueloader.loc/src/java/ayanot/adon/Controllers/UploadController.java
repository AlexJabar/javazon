/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ayanot.adon.Controllers;

import ayanot.adon.DB.DBFile;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import ayanot.adon.Helpers.Uploader;
import ayanot.adon.Helpers.PasswordHasher;
import ayanot.adon.business.UserFile;





@WebServlet("/upload")
@MultipartConfig() 
public class UploadController extends HttpServlet {

    

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        processRequest(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Part filePart = request.getPart("file");//достаем файл из формы
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();// костыль для рельного имени
        
         String uploadsFolder = "C:\\uploads"; // папка храненя файлов(хардкод!)
         File finalFolder = Uploader.pathGenerator(uploadsFolder); 
        
        try {
            String hashName = PasswordHasher.hashName(fileName);// хеш на основе имени файла (использую метод хеш. пароля)

            File file = new File(finalFolder, hashName + ".txt"); //создаем объект файла в безопасном формате тхт

        try (InputStream input = filePart.getInputStream()) {
            Files.copy(input, file.toPath());                     // копирует все байты input в файл
        }
        
        
        String url = file.getPath().replace(uploadsFolder, "");//оставляем путь из кореной папки файлов
        String oldName = fileName.replaceFirst("\\.[^.]+$", "");
        String extension = fileName.replaceFirst("(.+)(?=\\.[^.]+$)", "");
        String size = Uploader.formatFileSize(filePart.getSize());
        UserFile fileBean = new UserFile(hashName ,url, oldName, extension, size, "");
        DBFile.inserFile(fileBean);
        String link = request.getScheme() + "://" + request.getServerName() + ":" 
                                                  + request.getServerPort() + 
                                        request.getContextPath()+"/file/"+hashName;
        
        request.setAttribute("link", link);
        request.getRequestDispatcher("views/filePage.jsp").forward(request, response);
        
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UploadController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        processRequest(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
