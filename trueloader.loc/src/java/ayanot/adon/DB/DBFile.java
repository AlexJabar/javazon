/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ayanot.adon.DB;

import javax.persistence.EntityManager; 
import ayanot.adon.business.UserFile;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;


/**
 *
 * @author HACKER
 */
public class DBFile {
    
    public static void inserFile(UserFile file) {
        
        
        EntityManager em = DBconn.getEmf().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        try {
            trans.begin();
            em.persist(file);
            trans.commit();
        } catch (Exception ex){
            trans.rollback();
        } finally {
            em.close();
        }
    }
    
   public static UserFile fetchByName(String name) {
        EntityManager em = DBconn.getEmf().createEntityManager();
        String qString = "SELECT f FROM UserFile f WHERE f.name = :name";
        TypedQuery<UserFile> q = em.createQuery(qString, UserFile.class);
        q.setParameter("name", name);
        
        UserFile file= null;
        
        try {
            file = q.getSingleResult();
            
        } catch (NoResultException e) {
            System.out.println(e);
        } finally {
            em.close();
        }
        return file;
        
    }
    /*
    public static UserPerson fetchByName(String username) {
        EntityManager em = DBconn.getEmf().createEntityManager();
        String qString = "SELECT u FROM UserPerson u WHERE u.username = :username";
        TypedQuery<UserPerson> q = em.createQuery(qString, UserPerson.class);
        q.setParameter("username", username);
        
        UserPerson user = null;
        
        try {
            user = q.getSingleResult();
            
        } catch (NoResultException e) {
            System.out.println(e);
        } finally {
            em.close();
        }
        return user;
        
    }*/
    
   
    
}
