/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ayanot.adon.DB;

import javax.persistence.EntityManager; 
import ayanot.adon.business.UserPerson;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.persistence.NoResultException;
import ayanot.adon.Helpers.PasswordHasher;
import java.security.NoSuchAlgorithmException;


/**
 *
 * @author HACKER
 */
public class DBUser {
    
    public static void inserUser(UserPerson user) {
        ////////////////////- Хеширование и соление пароля- ////////////////
        try {
        String[] hashedPasswordAndSalt = PasswordHasher.hashAndSaltPassword(user.getPassword());
        user.setPassword(hashedPasswordAndSalt[0]);
        user.setSalt(hashedPasswordAndSalt[1]);

        } catch(NoSuchAlgorithmException ex) {
            System.out.print(ex);
        }
        ////////////////////////////////////////////////////////////////////
        
        EntityManager em = DBconn.getEmf().createEntityManager();
        EntityTransaction trans = em.getTransaction();
        try {
            trans.begin();
            em.persist(user);
            trans.commit();
        } catch (Exception ex){
            trans.rollback();
        } finally {
            em.close();
        }
    }
    
    public static UserPerson fetchByEmail(String email) {
        EntityManager em = DBconn.getEmf().createEntityManager();
        String qString = "SELECT u FROM UserPerson u WHERE u.email = :email";
        TypedQuery<UserPerson> q = em.createQuery(qString, UserPerson.class);
        q.setParameter("email", email);
        
        UserPerson user = null;
        
        try {
            user = q.getSingleResult();
            
        } catch (NoResultException e) {
            System.out.println(e);
        } finally {
            em.close();
        }
        return user;
        
    }
    
    public static UserPerson fetchByName(String username) {
        EntityManager em = DBconn.getEmf().createEntityManager();
        String qString = "SELECT u FROM UserPerson u WHERE u.username = :username";
        TypedQuery<UserPerson> q = em.createQuery(qString, UserPerson.class);
        q.setParameter("username", username);
        
        UserPerson user = null;
        
        try {
            user = q.getSingleResult();
            
        } catch (NoResultException e) {
            System.out.println(e);
        } finally {
            em.close();
        }
        return user;
        
    }
    
   
    
}
