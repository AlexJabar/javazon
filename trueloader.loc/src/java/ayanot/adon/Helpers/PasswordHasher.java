/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ayanot.adon.Helpers;

/**
 *
 * @author HACKER
 */
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.Base64;


public class PasswordHasher {
    public static String hashPassword(String password)
throws NoSuchAlgorithmException 
    {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.reset();
        md.update(password.getBytes());
        byte[] mdArray = md.digest();
        StringBuilder sb = new StringBuilder(mdArray.length * 2);
        for (byte b : mdArray) {
            int v = b & 0xff;
            if (v < 16) {
                sb.append ('0');
            }
            
            sb.append(Integer.toHexString(v));
        }
         return sb.toString(); 
         
    }
        
        public static String getSalt() {
            Random r = new SecureRandom();
            byte[] saltBytes = new byte[32];
            r.nextBytes(saltBytes);
            return Base64.getEncoder().encodeToString(saltBytes);
        }
        
        public static String[] hashAndSaltPassword(String password)
            throws NoSuchAlgorithmException {
                String salt = getSalt();
                String[] passwordAndSalt = {hashPassword(password + salt), salt};
                return passwordAndSalt;
                
            }
        
        public static String hashName(String name)
            throws NoSuchAlgorithmException {
                String salt = getSalt();
                 return hashPassword(name + salt);
            }
        
        
    }

