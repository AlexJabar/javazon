/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ayanot.adon.Helpers;

import ayanot.adon.DB.DBUser;
import ayanot.adon.business.UserPerson;
import java.util.ArrayList;
import java.util.HashMap;
/**
 *
 * @author HACKER
 */
public class UserValidator {
    
   private HashMap<String, ArrayList<String>> errors = new HashMap<>();
   private ArrayList<String> username;
   private ArrayList<String> email;
   private ArrayList<String> password;

    public HashMap<String, ArrayList<String>> getErrors() {
        return errors;
    }
   
   public boolean isFailed() {
       return !this.errors.isEmpty();
   }
   
   public void validate(UserPerson user, String rePassword) {
        // проверка совпадения паролей
       if (!user.getPassword().equals(rePassword)) {
          if (this.password == null) {
          	this.password = new ArrayList();
                this.errors.put("password", password);
          }
           this.password.add("Password does not match");
       }
       
       //проверка длинны пароля
       if (user.getPassword().length() < 6 || user.getPassword().length() > 30) {
          if (this.password == null) {
          	this.password = new ArrayList();
                this.errors.put("password", this.password);
          }
          this.password.add("Password length can only be from 6 to 30 symbols");
          
       }
       //проверка длинный имейла 
       if (user.getEmail().length() < 4 || user.getEmail().length() > 30) {
           if (this.email == null) {
          	this.email = new ArrayList();
                this.errors.put("email", this.email);
          }
          this.email.add("Email length can only be from 4 to 30 symbols");
           
       }
       //проверка длинны имени
       if (user.getUsername().length() < 4 || user.getUsername().length() > 30) {
           if (this.username == null) {
          	this.username = new ArrayList();
                this.errors.put("username", this.username);
          }
          this.username.add("Username length can only be from 4 to 30 symbols");
           
       }
       //проверка имейла на уникальность
        UserPerson userFetchedByEmail = DBUser.fetchByEmail(user.getEmail());
         if (userFetchedByEmail != null) {
            if (this.email == null) {
          	this.email = new ArrayList();
                this.errors.put("email", this.email);
            }
             this.email.add("This Email is already taken");
        }
       //проверка имени на уникальность
         UserPerson userFetchedByName = DBUser.fetchByName(user.getUsername());
         if (userFetchedByName != null) {
            if (this.username == null) {
          	this.username = new ArrayList();
                this.errors.put("username", this.username);
            }
             this.username.add("This Username is already taken");
        }
   }
    
}
