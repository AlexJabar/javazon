/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ayanot.adon.Helpers;

import ayanot.adon.DB.DBUser;
import ayanot.adon.business.UserPerson;
import java.security.NoSuchAlgorithmException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author HACKER
 */
public class Auth {
    
    public static boolean attempt(String email, String password, HttpServletRequest request) {
        
        UserPerson user = DBUser.fetchByEmail(email);
           
        if (user == null) {
            return false;
        }
        
        String passwordFormDB = user.getPassword();// это будем стравнивать с хешем(пароль+соль)
        String salt = user.getSalt();
        
        try {
            String passwordFormUser = PasswordHasher.hashPassword(password+salt);
            if (user.getEmail().equals(email) && passwordFormDB.equals(passwordFormUser)) {
                request.getSession().setAttribute("user", user.getUserId());
                return true;
            }
        } catch(NoSuchAlgorithmException ex) {
            System.out.println(ex);
        }
        
        return false;
        

    }
    
    
    
}
