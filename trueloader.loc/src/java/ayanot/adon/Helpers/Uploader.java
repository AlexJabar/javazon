
package ayanot.adon.Helpers;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Calendar;


public class Uploader {
    
    
    public static File pathGenerator(String root) {
        
        
        
        Calendar now = Calendar.getInstance();
            int y = now.get(Calendar.YEAR);
            int m = now.get(Calendar.MONTH)+1;
            String year = String.valueOf(y);
            String month = String.valueOf(m);

         File path = new File(root+"\\"+year+"\\"+month);
         
         if (path.exists() && path.isDirectory()) {
         } else 
             path.mkdirs();
         

         return path;

    }
    
    
   public static String formatFileSize(long size) {
    String hrSize = null;

    double b = size;
    double k = size/1024.0;
    double m = ((size/1024.0)/1024.0);
    double g = (((size/1024.0)/1024.0)/1024.0);
    double t = ((((size/1024.0)/1024.0)/1024.0)/1024.0);

    DecimalFormat dec = new DecimalFormat("0.00");

    if ( t>1 ) {
        hrSize = dec.format(t).concat(" TB");
    } else if ( g>1 ) {
        hrSize = dec.format(g).concat(" GB");
    } else if ( m>1 ) {
        hrSize = dec.format(m).concat(" MB");
    } else if ( k>1 ) {
        hrSize = dec.format(k).concat(" KB");
    } else {
        hrSize = dec.format(b).concat(" Bytes");
    }

    return hrSize;
}
    
    
}
