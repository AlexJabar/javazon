<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page = "../templates/header.jsp" />
        
        
        <div class="sign_form" align="right">
           <form action="signin" method="POST">
               
               <c:if test="${!empty error}">
                   <div class="error_mess">${error}</div>
               </c:if>
               
               <label>Email:</label>
               <input class="input_field" type="text" name="email" value="" /><br>
               
               <label>Password:</label>
               <input class="input_field" type="password" name="password" value="" /><br>
               
               <input type="submit" value="Sign In"/>
           </form>
        </div>
        
 <jsp:include page = "../templates/footer.jsp" />    