<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page = "../templates/header.jsp" />
        

        <div class="sign_form" align="right">
            <form action="signup" method="POST">
                
                <s:if test="errors.containsKey('username')">
                    <c:forEach var="hash" items="${errors['username']}">
                        <option class="error_mess" align="left"><c:out value="${hash}"/></option>
                    </c:forEach>   
                </s:if>
                
                <label>Name:</label>
                <input class="input_field" type="text" name="name" 
                       value="${!empty oldName ? oldName:""}" /><br>
                
                <s:if test="errors.containsKey('email')">
                    <c:forEach var="hash" items="${errors['email']}">
                        <option class="error_mess" align="left"><c:out value="${hash}"/></option>
                    </c:forEach> 
                </s:if>
                
                <label>Email: </label>
                <input class="input_field" type="text" name="email" 
                       value="${!empty oldEmail ? oldEmail:""}" /><br>
                
                <s:if test="errors.containsKey('password')">
                    <c:forEach var="hash" items="${errors['password']}">
                        <option class="error_mess" align="left"><c:out value="${hash}"/></option>
                    </c:forEach> 
                </s:if>
                
                <label>Password: </label>
                <input class="input_field" type="password" name="password" value="" /><br>
                <label>Repeat password: </label>
                <input class="input_field" type="password" name="passwordRep" value="" /><br>
                <input type="submit" value="Sign Up"/>
            </form>
            </div>
 <jsp:include page = "../templates/footer.jsp" />    
